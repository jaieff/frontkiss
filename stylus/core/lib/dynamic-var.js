// Need to relaunch gulp after modifications for working

var plugin = function(){
    return function(style){
        style.define('dynamicVar', function(name, value, prefix) {
          var finalName;

          if (prefix) {
            var finalName = prefix + name;
          }else {
            var finalName = name;
          }

          finalName = finalName.toString().replace(/['"]+/g, '')

          var variable = new style.nodes.Ident(finalName);

          variable.val = new style.nodes.Object();

          variable.val = value;
          this.currentScope.add(variable);

          // console.log("js: " + finalName);
        });

    }
}
module.exports = plugin;
