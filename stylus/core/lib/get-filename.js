// Need to relaunch gulp after modifications for working

var plugin = function(){
    return function(style){

      style.define('getFilename', function() {

          var path = require('path');
          var filename = path.basename(this.currentBlock.filename);

          return filename;
      });

    }
}
module.exports = plugin;
