var utils   = require('stylus').utils
  , nodes   = require('stylus').nodes
  , colors  = require('colors');



  var plugin = function(){
      return function(style){
          style.define('consoleLog', function(msg, type, init) {

            function getTiming() {
                var date = new Date();
                var hour = date.getHours();
                hour = (hour < 10 ? "0" : "") + hour;
                var min  = date.getMinutes();
                min = (min < 10 ? "0" : "") + min;
                var sec  = date.getSeconds();
                sec = (sec < 10 ? "0" : "") + sec;
                return hour + ":" + min + ":" + sec;
            }

            msg = msg.string;
            type = type.string;
            init = init.string;

            if (init == undefined) { init = env.name; }
            if (type == undefined || type == 'msg') { type = ' MSG '.bgCyan.black; msg = '['.cyan + getTiming().cyan + '] → '.cyan + msg.cyan; }
            if (type == 'error') { type = ' ERROR '.bgRed.black; msg = msg.red; }
            if (type == 'warn') { type = ' WARN '.bgYellow.black; msg = msg.yellow; }
            if (type == 'build') { type = ' BUILD '.bgYellow.black; msg = '['.yellow + getTiming().yellow + '] → '.yellow + msg.yellow; }
            if (type == 'dev') { type = ' DEV '.bgWhite.black; msg = '['.grey + getTiming().grey + '] → '.grey + msg.grey; }
            if (type == 'welcome')        { type = '   YO!   '.bgCyan.black; msg = msg.cyan; }
            if (type == 'welcome-blank')  { type = '         '.bgCyan.black; msg = msg.cyan; }
            if (type == 'help') { type = ' HELP '.bgCyan.black; msg = msg.cyan; }
            console.log('['.grey + init.grey + ']'.grey + ' → ' + type + ' ' + msg);
          });

      }
  }
  module.exports = plugin;
