// Need to relaunch gulp after modifications for working

var plugin = function(){
    return function(style){

        style.define('readDir', function (url) {
          var fs = require('fs'),
              p = url.toString().replace(/['"]+/g, ''),
              outDir;

          outDir = fs.readdirSync(p);
          return outDir
        });

    }
}
module.exports = plugin;
