// ---------------------------------------------
// - Help task
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog) {
    return function () {


          var stream = gulp.src('./')
            .on('end', function () {
              console.log('');
              consoleLog('TASKS', 'dev');
              consoleLog('→ gulp build        → Create all environements', 'help');
              consoleLog('→ gulp build-css    → Create environement only for css', 'help');
              consoleLog('→ gulp build-js     → Create environement only for js', 'help');
              consoleLog('→ gulp build-img    → Create environement only for images', 'help');
              console.log('');
              consoleLog('→ gulp watch        → Watch changement on all environements and compile what is needed to compile', 'help');
              console.log('');
              consoleLog('→ gulp clean        → Clean directories on all environements (WARN : all files and dir are destroyed)', 'help');
              consoleLog('→ gulp clean-css    → Clean directory on css environement (WARN : all files and dir are destroyed)', 'help');
              consoleLog('→ gulp clean-js     → Clean directory on js environement (WARN : all files and dir are destroyed)', 'help');
              consoleLog('→ gulp clean-img    → Clean directory on images environement (WARN : all files and dir are destroyed)', 'help');
              console.log('');
              consoleLog('FLAGS', 'dev');
              consoleLog('→ gulp {{task}} --prod      → Force production mode on any task', 'help');
              consoleLog('→ gulp {{task}} --silent    → Shut down default gulp msgs (but keep on framework)', 'help');
              console.log('');
          });

          return stream;

    };
};
