/*///////////////////////
// Say welcome
///////////////////////*/

module.exports = function (gulp, plugins, env, consoleLog) {
    return function () {

          var quotes = [
            "Innovation distinguishes between a leader and a follower.",
            "Where there is no vision, the people perish.",
            "You don’t need a title to be a leader.",
            "Anyone can hold the helm when the sea is calm.",
            "Whatever you are, be a good one.",
            "If you do it right, it will last forever.",
            "Everything is designed. Few things are designed well.",
            "Leave it better than you found it.",
            "Simplicity, wit, and good typography.",
            "The details are not the details. They make the design.",
            "Design is so simple. That’s why it’s so complicated.",
            "Put hot triggers in the path of motivated people.",
            "Good design is obvious. Great design is transparent.",
            "Do good work for good people.",
            "Jaieff is always there. And see you !",
            "We do not remember days, we remember moments.",
            "Focus on what matters and let go of what doesn’t.",
            "Work hard. Dream big.",
            "Life is a one time offer, use it well.",
            "Everything happens for a reason.",
            "Be the change you wish to see in the world.",
            "Love the life you live, and live the life you love.",
            "Try and fail, but never fail to try.",
            "Everything you can imagine is real.",
            "I can, therefore I am.",
            "Wherever you go, go with all your heart",
            "Action is the foundational key to all success.",
            "Life is trying things to see if they work.",
            "Don’t regret the past, just learn from it.",
            "Believe you can and you’re halfway there.",
            "The best revenge is massive success.",
            "Right foot, right angle !"
          ];

          var stream = gulp.src('./')
            .on('end', function () {
              console.log('');
              console.log('');
              consoleLog('', 'welcome-blank');            // Send init message
              consoleLog(quotes[Math.floor(Math.random()*quotes.length)], 'welcome');            // Send init message
              consoleLog('', 'welcome-blank');            // Send init message
            });

          return stream;

    };
};
