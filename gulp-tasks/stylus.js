// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, stylus, plumber, notify, gulpif, beep, autoprefixer, lost, poststylus, sourcemaps, argv, rename, reload) {
    return function () {

        var stylNbrElements   = 1,
            stylComponents    = JSON.parse(require('fs').readFileSync('./stylus/app-components.json', 'utf8'));
            stylSite          = JSON.parse(require('fs').readFileSync('./stylus/app-site.json', 'utf8'));

        if (argv.globalStyl == undefined) {
          argv.globalStyl = true;
        }

        // Create name of files
        var stylNames           = [ 'app', 'init'],
            stylComponentsNbr   = 0,
            stylSiteNbr         = 0,
            stylComponentsNames = [],
            stylSiteNames       = [],
            stylFinalNames      = [];



        // Check components
        if (argv.global) {
          // One file
          stylFinalNames = stylNames;
          env.style.compress = true;
          argv.stylMaps = false;
        }else if (argv.globalStyl) {

          // All files
          stylNbrElements = 2;

          for(key in stylComponents) {
              if (stylComponents[key]) {
                stylNames.push(key);
                stylComponentsNames.push(key);
                stylNbrElements++;
                stylComponentsNbr++;
              }
          }
          for(key in stylSite) {
              if (stylSite[key]) {
                stylNames.push(key);
                stylSiteNames.push(key);
                stylNbrElements++;
                stylSiteNbr++;
              }
          }
          stylFinalNames = stylNames;
        }else {

          // Component/site file + main file
          for(key in stylComponents) {
              if (stylComponents[key]) {
                stylNames.push(key);
                stylComponentsNames.push(key);
                stylNbrElements++;
                stylComponentsNbr++;
              }
          }
          for(key in stylSite) {
              if (stylSite[key]) {
                stylNames.push(key);
                stylSiteNames.push(key);
                stylNbrElements++;
                stylSiteNbr++;
              }
          }
          stylNbrElements = 2;
          stylFinalNames.push(stylNames[0]);

          var watchFileName = argv.watchFile.replace(/^.*[\\\/]/, '');
          watchFileName = watchFileName.replace(".styl", "");
          watchFileName = watchFileName.replace(".overwrite", "");
          watchFileName = watchFileName.replace(".config", "");
          stylFinalNames.push(watchFileName);
        }



        if (argv.watchStylus == true) {
          //launch nbr of tasks
          var stream = gulp.src('./')
            .on('end', function () {
              consoleLog('Waiting for CSS compilation...', 'dev');
              // Here we are
              for (var i = 0; i < stylNbrElements; i++) {

                if (i == 0) {
                  devOut = '';
                }else {
                  devOut = 'css-includes' }

                if ( i < 2) {
                  stylPrefix = '';
                }else if (i <= (stylComponentsNbr + 1) ) {
                  stylPrefix = 'component-';
                } else {
                  stylPrefix = 'site-'; }

                if (i == 1 && argv.stylType == 'components' && argv.global == false && argv.globalStyl == false) {
                  stylPrefix = 'component-';
                }else if (i == 1 && argv.stylType == 'site' && argv.global == false && argv.globalStyl == false) {
                  stylPrefix = 'site-';
                }


                gulp.src(env.style.mainFile)                            // What to load as stylus file(s)
                .pipe( plumber( function (error) {                      // if error detected
                  beep();                                               // What to load as stylus file(s)
                  consoleLog('Aïe, something is broken...', 'error');   // Send message
                  console.log(error.message);                           // Stylus message
                  this.emit('end');                                     // End task here
                }))
                .pipe(gulpif( argv.stylMaps ,sourcemaps.init()))        // Sourcemap init if used
                .pipe(stylus({
                  use       :                                           // Use things inside stylus
                  [ poststylus([                                        // Post stylus launcher
                    'lost',                                             // Use lost framework
                    'autoprefixer'                                      // Use autoprefixer
                  ])],                                                  // Add all Post CSS here
                  define: {
                    'envName'                   : env.name,
                    'fileName'                  : stylPrefix + stylFinalNames[i] + '.css',
                    'envGlobal'                 : argv.global,
                    'envGlobalStyl'             : argv.globalStyl,
                    'stylType'                  : argv.stylType,
                    'stylComponentFinalNames'   : watchFileName,
                    'stylSiteNames'             : stylSiteNames,
                    'stylCompilationNbr'        : i,
                    'lastIteration'             : stylNbrElements,
                    'stylFinalNames'            : stylFinalNames,
                    'stylComponentsNbr'         : stylComponentsNbr
                  },
                  compress  : env.style.compress                        // Compression if actived
                }))
                .pipe(gulpif( argv.stylMaps, sourcemaps.write(env.style.maps.mapOut)))             // Write maps where you want
                .pipe(rename(stylPrefix + stylFinalNames[i] + '.css'))
                .pipe(gulp.dest(env.style.cssOut + devOut))                      // Destination of CSS
                .on('end', function(newFile) {

                })
                .on('error', function() {
                  return consoleLog('Something wrong with : ' + stylPrefix + stylNames[i], 'error');
                });
                if (i == 0) {

                  console.log('');
                }
                //consoleLog('css compiled : ' + stylPrefix + stylFinalNames[i] + '.css');


              }

          });
          return stream;
        }
    };
};
