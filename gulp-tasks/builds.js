// ---------------------------------------------
// - Help task
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv) {
    return function () {

          argv.watchVars      = true;
          argv.watchStylus    = true;
          argv.globalStyl     = true;
          argv.watchJs        = true;
          argv.watchJsType    = 'all';
          argv.watchImg       = true;


          var stream = gulp.src('./')
            .on('end', function () {

              console.log('');
              consoleLog('Waiting for building environement...', 'build');
              console.log('');
          });

          return stream;

    };
};
