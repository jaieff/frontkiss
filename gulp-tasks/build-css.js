// ---------------------------------------------
// - Help task
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv) {
    return function () {

          argv.watchVars      = true;
          argv.watchStylus    = true;
          argv.globalStyl     = true;
          argv.watchJs        = false;
          argv.watchJsType    = false;
          argv.watchImg       = false;


          var stream = gulp.src('./')
            .on('end', function () {

              console.log('');
              consoleLog('Waiting for building CSS environement...', 'build');
              console.log('');
          });

          return stream;

    };
};
