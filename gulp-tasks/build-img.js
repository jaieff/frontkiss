// ---------------------------------------------
// - Help task
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv) {
    return function () {

          argv.watchVars      = false;
          argv.watchStylus    = false;
          argv.globalStyl     = false;
          argv.watchJs        = false;
          argv.watchJsType    = false;
          argv.watchImg       = true;


          var stream = gulp.src('./')
            .on('end', function () {

              console.log('');
              consoleLog('Waiting for building image environement...', 'build');
              console.log('');
          });

          return stream;

    };
};
