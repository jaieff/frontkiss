// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif) {
    return function () {

        // Filter .DS_Store function for emptyDir
        function filter(filepath) {
            return !/\.DS_Store$/i.test(filepath);
        }

        // Check if folder is empty
        emptyDir(env.style.cssOut, filter, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                argv.cleanStyle = result;
            }
        });

        // Stream
        var stream = gulp.src('./')
            .on('end', function(){

                del([env.style.cssOut + '**/*'], {force: true});
                del([env.js.jsOut + '**/*'], {force: true});
                del([env.img.imgOut + '**/*'], {force: true});

            });


      return stream;


    };
};
