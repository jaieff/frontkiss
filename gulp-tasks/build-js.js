// ---------------------------------------------
// - Help task
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv) {
    return function () {

          argv.watchVars      = false;
          argv.watchStylus    = false;
          argv.globalStyl     = false;
          argv.watchJs        = true;
          argv.watchJsType    = 'all';
          argv.watchImg       = false;


          var stream = gulp.src('./')
            .on('end', function () {

              console.log('');
              consoleLog('Waiting for building JS environement...', 'build');
              console.log('');
          });

          return stream;

    };
};
