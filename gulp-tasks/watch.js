// ---------------------------------------------
// - Watch module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv) {
    return function () {

        var rules           = [],                                             // Init array of watch urls
            path            = require("path");                                // Get the plugin requirement "path"


        for(value in env.watchRules) {
            rules.push(env.watchRules[value]);                                // Loop watch URLs
        }

        var stream = gulp.watch(rules, gulp.parallel('stylus', 'js-components', 'js-lib', 'img-optim')).on('change', function(file, filepath) {

          // Get file name + path
          argv.watchFile = file;

          // Check if file is stylus dependencie
          if ( /^stylus\/(|$)/.test(file) ) {
            argv.watchStylus = true;

            // if it's component
            if ((/(|^)\/components\/(|$)/.test(file) || /(|^)\/themes\/(|$)/.test(file)) && !/(|^)\/themes\/config.themes.styl(|$)/.test(file)) {
                argv.stylType   = 'components';
                argv.globalStyl = false;

            // if it's site
            }else if (/(|^)\/site\/(|$)/.test(file)) {
                argv.stylType   = 'site';
                argv.globalStyl = false;

            // if it's global
            }else {
              argv.cssOut       = '';
              argv.stylType     = '';
              argv.globalStyl   = true;
            }

          } else {
            argv.watchStylus    = false;
          }

          // Check if file is js dependencie
          if ( /^js\/(|$)/.test(file) ) {
            argv.watchJs     = true;
            if (/lib(|$)/.test(file)) {
              argv.watchJsType = 'lib';
            }else if (/components(|$)/.test(file)) {
              argv.watchJsType = 'components';
            }else {
              argv.watchJsType = 'all';
            }
          } else {
            argv.watchJs     = false;
          }

          // Check if file is vars dependencie
          if ( /^vars\/(|$)/.test(file) ) {
            argv.watchVars      = true;
            argv.watchStylus    = true;
            argv.globalStyl     = true;
            argv.watchJs        = true;
            argv.watchJsType    = 'all';

          } else {
            argv.watchVars      = false;
          }

          if ( /^img\/(|$)/.test(file) ) {
            argv.watchImg      = true;
          } else {
            argv.watchImg      = false;
          }

        });
        console.log('');
        consoleLog(argv.stylGlobalMsg, 'build');                             // Send init message
        consoleLog('you\'re watching for : ', 'dev');                       // Send init message

        for (var k in env.watchRules){
            if (env.watchRules.hasOwnProperty(k)) {                         // Send callback message
                console.log(("               → ").grey + env.watchRules[k] + (" → ").grey + k.grey);
            }
        }

        return stream;

    };
};
