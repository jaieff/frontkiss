// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif) {
    return function () {

        argv.silent = true;

        // Filter .DS_Store function for emptyDir
        function filter(filepath) {
            return !/\.DS_Store$/i.test(filepath);
        }

        // Check if folder is empty
        emptyDir(env.style.cssOut, filter, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                argv.cleanStyle = result;
            }
        });

        console.log('');
        console.log('');

        // Stream
        var stream = gulp.src(env.style.cssOut + '**/*', {read: true})
            .pipe(
              gulpif( !argv.cleanStyle,
                prompt.confirm({
                  message: '[' + (env.name).grey + '] → ' + ' WARN '.bgYellow.black + ' ' + 'Do you realy want to destroy CSS output files'.yellow,
                  default: true
            })))
            .on('end', function(){
              console.log('');
              if (!argv.cleanStyle) {
                del([env.style.cssOut + '**/*'], {force: true}).then( function () {
                    consoleLog(env.style.cssOut + ' successfuly cleaned', 'build')
                    console.log('');
                });

              } else {
                consoleLog('Nothing to destroy.', 'dev');
                console.log('');
              }
            });


      return stream;


    };
};
