// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif) {
    return function () {

        argv.silent = true;

        // Filter .DS_Store function for emptyDir
        function filter(filepath) {
            return !/\.DS_Store$/i.test(filepath);
        }

        // Check if folder is empty
        emptyDir(env.img.imgOut, filter, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                argv.cleanImg = result;
            }
        });

        console.log('');
        console.log('');

        // Stream
        var stream = gulp.src(env.img.imgOut + '**/*', {read: true})
            .pipe(
              gulpif( !argv.cleanImg,
                prompt.confirm({
                  message: '[' + (env.name).grey + '] → ' + ' WARN '.bgYellow.black + ' ' + 'Do you realy want to destroy output Img files'.yellow,
                  default: true
            })))
            .on('end', function(){
              console.log('');
              if (!argv.cleanImg) {
                del([env.img.imgOut + '**/*'], {force: true}).then( function () {
                    consoleLog(env.img.imgOut + ' successfuly cleaned', 'build')
                    console.log('');
                });
              } else {
                consoleLog('Nothing to destroy.', 'dev');
                console.log('');
              }
            });


      return stream;


    };
};
