// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif) {
    return function () {

        argv.silent = true;

        // Filter .DS_Store function for emptyDir
        function filter(filepath) {
            return !/\.DS_Store$/i.test(filepath);
        }

        // Check if folder is empty
        emptyDir(env.style.cssOut, filter, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                argv.cleanStyle = result;
            }
        });

        emptyDir(env.js.jsOut, filter, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                argv.cleanJS = result;
            }
        });

        emptyDir(env.img.imgOut, filter, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                argv.cleanImg = result;
            }
        });

        console.log('');
        console.log('');

        // Stream
        var stream = gulp.src([env.style.cssOut + '**/*', env.js.jsOut + '**/*', env.img.imgOut + '**/*' ], {read: true})
            .pipe(
              gulpif( !argv.cleanStyle || !argv.cleanJs || !argv.cleanImg ,
                prompt.confirm({
                  message: '[' + (env.name).grey + '] → ' + ' WARN '.bgYellow.black + ' ' + 'Do you realy want to destroy all output files'.yellow,
                  default: true
            })))
            .on('end', function(){
              console.log('');
              if (!argv.cleanStyle) {
                del([env.style.cssOut + '**/*'], {force: true}).then( function () {
                    consoleLog(env.style.cssOut + ' successfuly cleaned', 'build')
                    console.log('');
                });
              } else {
                consoleLog('Nothing to destroy in Css output.', 'dev');
                console.log('');
              }

              if (!argv.cleanJS) {
                del([env.js.jsOut + '**/*'], {force: true}).then( function () {
                    consoleLog(env.style.cssOut + ' successfuly cleaned', 'build')
                    console.log('');
                });
              } else {
                consoleLog('Nothing to destroy in Js output.', 'dev');
                console.log('');
              }

              if (!argv.cleanImg) {
                del([env.img.imgOut + '**/*'], {force: true}).then( function () {
                    consoleLog(env.img.imgOut + ' successfuly cleaned', 'build')
                    console.log('');
                });
              } else {
                consoleLog('Nothing to destroy in Img output.', 'dev');
                console.log('');
              }
            });


      return stream;


    };
};
