/*///////////////////////
// Say welcome
///////////////////////*/

module.exports = function (gulp, plugins, env, consoleLog) {
    return function () {


          var stream = gulp.src('./')
            .on('end', function () {
              console.log('');
              console.log('');
              consoleLog('All is done. Enjoy!', 'build');            // Send init message
              console.log('');
              console.log('');
            });

          return stream;

    };
};
