// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, plumber, notify, gulpif, beep, argv, imagemin) {
    return function () {


        if (argv.watchImg) {

          consoleLog('Waiting for img compilation...', 'dev');
          var stream = gulp.src('./img/**/*')
          		.pipe(gulpif(argv.global, imagemin({
                  progressive: env.img.progressiveJpg,
                  interlaced: env.img.interlacedGif,
                  multipass: env.img.multipassSvg,
                  optimizationLevel: env.img.optimizationLevelPng
              })))
          		.pipe(gulp.dest(env.img.imgOut))
              .on('end', function () {
                console.log('');
                consoleLog('Images moved and optimised : ' + env.img.imgOut, 'msg');
                consoleLog('Images optimisation ended!', 'dev');
              });


          return stream;
        }
    };
};
