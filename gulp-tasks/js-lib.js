// ---------------------------------------------
// - Stylus module
// ---------------------------------------------

module.exports = function (gulp, plugins, env, consoleLog, stylus, plumber, notify, gulpif, beep, sourcemaps, argv, rename, reload) {
    return function () {
        var concat        = require("gulp-concat");
        var uglify        = require("gulp-uglify");
        var jsLib  = JSON.parse(require('fs').readFileSync('./js/app-lib.json', 'utf8'));


        var jsLibFinal = [];

        if (argv.jsMaps == undefined) {
          argv.jsMaps = env.js.maps.use
        }

        if (argv.global) {
          argv.jsMaps = false
        }

        if (argv.global || env.js.compress) {
          argv.jsMini = true;
        }else {
          argv.jsMini = false;
        }

        for(key in jsLib) {
            if (jsLib[key]) {
              jsLibFinal.push('./js/lib/' + key + '.js');
            }
        }

        if (argv.watchJs == true && (argv.watchJsType == 'lib' || argv.watchJsType == 'all')) {
          //launch nbr of tasks
          consoleLog('Waiting for Js compilation...', 'dev');
          var stream = gulp.src('./')
            .on('end', function () {

              // Here we are
              gulp.src(jsLibFinal)                            // What to load as stylus file(s)
              .pipe(gulpif( argv.jsMaps ,sourcemaps.init()))        // Sourcemap init if used
              .pipe(concat('lib.js'))
              .pipe(gulpif( argv.jsMini ,uglify()))
              .pipe( plumber( function (error) {                      // if error detected
                beep();                                               // What to load as stylus file(s)
                consoleLog('Aïe, something is broken...', 'error');   // Send message
                console.log(error.message);                           // Stylus message
                this.emit('end');                                     // End task here
              }))

              .pipe(gulpif( argv.jsMaps, sourcemaps.write(env.js.maps.mapOut)))             // Write maps where you want
              .pipe(gulp.dest(env.js.jsOut))                      // Destination of CSS
              //.on('end', function() { })
              .on('error', function() {
                //consoleLog('Something wrong with : ' + stylPrefix + stylNames[i], 'error');
              }).on('end', function () {

                console.log('');
                consoleLog('JS libraries compiled : lib.js' , 'msg');
                consoleLog('JS libraries compilation ended!', 'dev');

              });

          });
          return stream;
        }
    };
};
