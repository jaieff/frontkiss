# **1 Introduction**
This framework is defined to front-end. It give a way to organise all front-end assets (JS, CSS (stylus) and images) into one place and an easy canvas to develop quickly and optimise production deployment.



# **2 Installation**
To use this framework, first you need to install **globally** :

+ [NodeJS 4.4+](https://nodejs.org/en/download/)


+ NPM (installed with NodeJS)


+ Gulp-Cli 4.0+ (line command tool for Gulp)
		`npm install gulpjs/gulp-cli#4.0 -g`
		
+ Gulp 4.0+
		`npm install gulpjs/gulp.git#4.0 --save-dev`

Before to run the framework, be sure to install dev dependencies  with NPM (install it globally to have less dependencies to your project dir)
	`npm install`



# **3 Environment file**
The env file `./env.frontkiss.json` is the main file to configure the basics

+ `name` > `{{ string }}` : define name of project

+ `statement` > `[[ dev || prod ]]` : define if the project is in development or in production

+ `watchRules` > `{{ n lines with > name : urls }}` : define dir when the watch task look for changement. 


+ 	`style`: Define CSS/Stylus environment

	+ `mainFile` > `./stylus/app.styl` : master file of stylus part (don’t change)

	+ `cssOut` > `{{ dir dir (relative to root project path) }}` : output css dir

	+ `compress` > `[[ boolean ]]` : define compression for dev statement (always compressed in prod)

	+ `maps`: maps for dev (never used in prod statement)

		+ `use` > `[[ boolean ]]` : define if maps are used for css

		+ `mapOut` > `{{ path (relative to ccOut) }}` : where to write maps (blank = inside file)


+ 	`js`: Define js environment

	+ `jsOut` > `{{ dir dir (relative to root project path) }}` : output js dir

	+ `compress` > `[[ boolean ]]` : define compression for dev statement (always compressed in prod)

	+ `maps`: maps for dev (never used in prod statement)

		+ `use` > `[[ boolean ]]` : define if maps are used for css

		+ `mapOut` > `{{ path (relative to ccOut) }}` : where to write maps (blank = inside file)


+ 	`img`: Define images environment

	+ `imgOut` > `{{ dir (relative to root project path) }}` : output js dir

	+ `progressiveJpg` > `[[ boolean ]]` : compression for Jpeg (only prod statement)

	+ `interlacedGif` > `[[ boolean ]]` : compression for Gif (only prod statement)

	+ `multipassSvg` > `[[ boolean ]]` : compression for Svg (only prod statement)

	+ `optimizationLevelPng` > `[[ 0 > 7 ]]` : compression for Png. Higher is best optimisation but slower (recommandation is 2) (only prod statement)



# **4 Gulp Tasks**
Launch Gulp tasks to control the framework. Don’t forget to create the environment the first time with `gulp build` and also every time you get a new version of a project.


### **4.1 Build tasks**
`gulp build`        → Create all environements

`gulp build-css`    → Create environement only for css

`gulp build-js`     → Create environement only for js

`gulp build-img`    → Create environement only for images


### **4.2 Watch task**
`gulp watch`        → Watch changement on all environements and compile what is needed to compile

### **4.3 Clean tasks**
`gulp clean`        → Clean directories on all environements (WARN : all files and dir are destroyed)

`gulp clean-css`    → Clean directory on css environement (WARN : all files and dir are destroyed)

`gulp clean-js`     → Clean directory on js environement (WARN : all files and dir are destroyed)

`gulp clean-img`    → Clean directory on images environement (WARN : all files and dir are destroyed)


### **4.4 Flags**
`gulp {{task}} --prod`      → Force production mode on any task

`gulp {{task}} --silent`    → Shut down default gulp msgs (but keep on framework)



# **5 Vars**
`vars` dir is used to have a collection of Json with all commons variables for stylus and JavaScript both. Use son like classical way.

### **5.1 intro to Breaks-points**
`vars/bp` has a special organisation to work with stylus and create it has to sub dir to define breaks-points inside or outside the injection flow.

### **5.2 In the flow**
`vars/bp/in/*.json` define breaks-points used with in function `toBp()` on a CSS property ex : `height: toBp(‘l*3’)` (more into Stylus part)

You can create all breakpoints you need, but you need one minimum (+ map file)

+ `name` > `{{ string || null }}` : name of your breakpoint 

+ `prefix` > `{{ string || null }}` : name to call this break-point into the `toBp()` function > `toBp({ … }, ‘prefix’ )` (**Warn, if you want to call this breakpoint**)

+ `break` : rules to write media queries

	+ `break-max` > `{{ dimension with unit (ex: 960px) || null }}` : is max-width

	+ `break-min` > `{{ dimension with unit (ex: 960px) || null }}` : is min-width

+ `container` : definition for the `.container` class

	+ `max-width` > `{{ dimension with unit (ex: 960px) }}` : is width of element

	+ `padding` > `{{ unit }}` : define the padding of container. **unit is multiplied by the line** to find the definitive padding size.

+ `typo` : definition for the `.container` class

	+ `font-size` > `{{ dimension with unit (ex: 16px) }}` : is the **font-size** for this break-point

	+ `line` > `{{ dimension with unit (ex: 16px) }}` : is the **line-height** for this break-point

The file `_map-in.json` is the model of this kind of Json. It used to merge on a breakpoint to aboard system errors.

### **Out the flow**
`vars/bp/out/*.json` define breaks-points not rendered  in function `toBp()` on a CSS property ex : `height: toBp(‘l*3’)` (more into Stylus part) but can be called anywhere.

You can create all breakpoints you need, you don’t need files if you don’t have special bp (+ map file)

+ `name` > `{{ string || null }}` : name of your breakpoint 

+ `prefix` > `{{ string || null }}` : name to call this break-point into the `toBp()` function > `toBp({ … }, ‘prefix’ )` (**Warn, if you want to call this breakpoint**)

+ `call-vars-of` > `{{ break-point name “in flow” || null }}` : if you want to copy vars of a break-point into the flow (ex : for typography rules, container…), you can reference the name of it. all vars will copied for this breakpoint (not for name, prefix and break rules).

+ `break` : rules to write media queries

	+ `break-max` > `{{ dimension with unit (ex: 960px) || null }}` : is max-width

	+ `break-min` > `{{ dimension with unit (ex: 960px) || null }}` : is min-width



# **5 Sylus**
Stylus is the biggest part of the framework. All vars are imported into it and have a specific workflow based on break-point injections and definitions by components.

`stylus/app.styl` is the main file stylus part. Work with `stylus/core` to manage the app

### **5.1 Core**
`stylus/core` : globally, dont touch, it’s the brain of app and do these things :
+ Speak with gulp to launch correct compilation system on right files
+ Import vars in the proper way
+ Initialisation, stock and render for `toBp()` function
+ Extend Stylus with NodeJS for some specific tasks

### **5.2 App config files**
To manage stylus framework, on root of stylus dir, you have four JSON who organise breakpoints, vars, components and sites parts.

**In all these JSON, the framework give always more importance to lines who are on top. So it launch compilation on logical human reading. The first line is interpreted, next the second, third, etc…**

**Inside JSONs, keys are ALWAYS the name of the file (without the extension) and the value is a boolean (or a string for vars). Exemple : you define a new stylus component called `header.styl`, in your JSON, you called this component with a line like this : `”header” : {{ boolean }}`**

#### **5.2.1 Breakpoints configuration**
`stylus/app-breakpoints.json` organise yours `in` and `out` breakpoints.

+ `bp-in-flow: { … }` > breakpoints **in the flow**
+ `bp-out-flow: { … }` > breakpoints **out the flow**

inside, you call your breakpoints define in `vars/bp/…` line by line. the structure is :
`”{{ name of breakpoint file }}” : {{ boolean }}` > if the boolean is true, the breakpoint is used. If it’s false, it’s not used.

#### **5.2.2 Component configuration**
`stylus/app-components.json` organise your components.

You call your components define in `stylus/components/` like this :
+ `”{{ name of component file }}” : {{ boolean }}` > if the boolean is true, the component is used. If it’s false, it’s not used.

#### **5.2.3 Site parts configuration**
`stylus/app-site.json` organise your site parts.

You call your breakpoints site parts in `stylus/site/` like this :
+ `”{{ name of site part file }}” : {{ boolean }}` > if the boolean is true, the site part is used. If it’s false, it’s not used.


#### **5.2.4 Vars configuration**
`stylus/app-vars.json` organise your variables. It’s a little different of all app configuration.

You call your breakpoints site parts in `stylus/components/` like this :
`”{{ name of var file }}” : {{ name of variable }}` > the value represent the name of the final hash inside stylus.

+ exemple : for `colours.json` > `”colors” : “c”` : give all variables of `colours.json` in `c` hash inside stylus. 
+ You can know use variables like this : `c.my-color`

### **5.3 Initialisation of CSS**
The framework give a way to have a css part who is compiled before component and site part (exemple: reset css, master class…). It’s mastered by the `stylus/init/init.styl` file. Actually, the `reset css` imported there and `.container` file based on break-points configurations. 

### **5.4 Components and themes structures**
A component is a part of framework who is destined to be reusable between projects (forms, buttons…). A component use always 3 stylus files (if these 3 files doesn’t exist it won’t compile the framework.

+ `stylus/components/{{name}}.styl` > is the css definition of the component. It could be the same for every projects.

Next, we use themes to configure these definitions and overwrite on definitions. Theme logic are defined in `stylus/themes/{{theme-name}}`. By default, every themes uses `stylus/themes/base/` but you can modify that with configuration in `stylus/themes/config.themes.styl`. This file define which component use which theme for compilation and work like this : 
+ `themes = { … } `  to define a specific theme for a component, add the name of component as `key` and `theme name`as value. Like this : 
+ `”{{ forms }}”: “{{theme-name}}”`.

Inside a theme folder, you need two file for components : 

+ `themes/{{theme-name}}/{{component-name}}.config.styl` > is the css definition of the component. Ideally, it contain only variables who are used in definition file of component.

+ `themes/{{theme-name}}/{{component-name}}.overwrite.styl` > allow you to overwrite css definitions (give a way to modify definitions without modify definitions).

### **5.5 Site parts structures**
Site parts are specific to a project and organised like you want with the config file. Ideally, pages (or sections for big pages) should be spared in distinct files.

### **5.6 Container class**
The `.container` class is the main class and launched in `stylus/init/init.styl` file. It’s the best usage to use content  who fit with your media queries.

### **5.7 Access to break points**
The framework allow you to use the JSON configurations of breakpoints in stylus with two specific vars. Ideally use these vars with the fonction `toBp()` 

+ `bp` > access to the current break point who is `in flow`

	+ exemple: `bp.name`


+ `bpOut` > access to the current break point who is `out flow`

	+ exemple: `bpOut.call-of-vars`


### **5.8 Font-size and line-height**
As a good designer, every good design should be based on a vertical rhythm based on the font-size and his line-height. 

Every break point configuration has these two vars into. You can use it everywhere and should be use with the function `toBp()` to be injected into breakpoints.

+ `fs` > is for font-size

+ `l` > is for the line (line-height)


### **5.9 Injections into break points**
The break points injection is used with the function `toBp()` who have two canvas to be launched:

+ On a css property > `{{ property }}: toBp(‘{{ value }}’)`: this method inject directly on property but keep in the cache the value to write into all in flow break points. 

Exemple : with 3 `in-flow` breakpoints, the following code :

```stylus
 #foo
	 bar: tobp(‘fs’)
	 bad: tobp(‘l’)
 ```
 
 Will return :
 
 ```css
 #foo {
	 bar: 16px;
	 baz: 24px;
 }
 
 @media (max-width: 768px) {
	 #foo {
		 bar: 15px;
		 baz: 22px;
	 }
 }
 
 @media (max-width: 480px) {
	 #foo {
		 bar: 14px;
		 baz: 19px;
	 }
 }
 ```
 
+ Directly in a selector : 

+ `toBp({  {{ hash }}  }, ‘prefix’)`: this method inject on a specific breakpoint when the render for itself is launched. 

Exemple : with a `header` breakpoint, the following code :

```stylus
 #foo
	 tobp({ bar: ‘fs’, baz: ‘l’ }, ‘header’)
 ```
 
 Will return :
 
 ```css
 @media (max-width: 654px) {
	 #foo {
		 bar: 14px;
		 baz: 19px;
	 }
 }
```

# **6 Javascript**
The Javascript part of framework is divided in two parts. : `libraries` and `components`.

**Inside js, it’s highly recommended to use variables of the `vars` folder to have more coherence between CSS and JS part.**

### **6.1 Libraries**
You organise all your libraries files into `js/lib` dir like you want. The file `js/app-lib.json` define the order of call and if it’s used into app like this :

”{{ name of library file }}” : {{ boolean }} > if the boolean is true, the library is used. If it’s false, it’s not used.

### **6.2 Components**
You organise all your components files into `js/components` dir like you want. The file `js/app-components.json` define the order of call and if it’s used into app like this :

”{{ name of component file }}” : {{ boolean }} > if the boolean is true, the library is used. If it’s false, it’s not used.

# **6 Images & other assets**
You can organise your assets like you want inside `img` folder. All content of this dir is copied into destination dir defined into environment file (and optimised in prod mode).