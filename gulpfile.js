'use strict';
// Gulp
var gulp          = require('gulp'),                              // Gulp
    plugins       = require('gulp-load-plugins')(),               // Gulp tasks as plugins (into gulp-tasks)
    gutil         = require('gulp-util');

// Set Gulp dependencies
var stylus        = require('gulp-stylus'),                       // Stylus
    plumber       = require('gulp-plumber'),                      // Plumber
    notify        = require('gulp-notify'),                       // Improve gulp notifications
    gulpif        = require('gulp-if'),                           // Add conditions in gulp
    beep          = require('beepbeep'),                          // BeepBeep
    autoprefixer  = require('autoprefixer'),                      // Autoprefixer
    lost          = require('lost'),                              // Lost framework
    poststylus    = require('poststylus'),                        // PostStylus (So good !)
    colors        = require('colors'),                            // Console colors
    sourcemaps    = require('gulp-sourcemaps'),                   // Sourcemaps Support
    argv          = require('yargs').argv,                        // Arguments across plugins
    del           = require('del'),                               // Delete files and folders
    prompt        = require('gulp-prompt'),                       // Prompt in terminal
    emptyDir      = require('empty-dir'),                         // Check if dir is empty
    rename        = require("gulp-rename"),                       // Rename output files
    imagemin      = require("gulp-imagemin");                     // Minify and move images

// Set environement
var env           = require('./env.frontkiss.json');              // Import environment file

// Variable used in gulp
// argv.prod              --> Define if prod env or flag is actived
// argv.global        --> Define styl compilation is global or local
// argv.stylGlobalMsg     --> Define the msg to know compilation style
// argv.stylMaps          --> Define usage of SourcesMaps

function getTiming() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    return hour + ":" + min + ":" + sec;
}

// Console call function
function consoleLog(msg, type, init) {
    if (init == undefined) { init = env.name; }
    if (type == undefined || type == 'msg') { type = ' MSG '.bgCyan.black; msg = '['.cyan + getTiming().cyan + '] → '.cyan + msg.cyan; }
    if (type == 'error') { type = ' ERROR '.bgRed.black; msg = msg.red; }
    if (type == 'warn') { type = ' WARN '.bgYellow.black; msg = msg.yellow; }
    if (type == 'build') { type = ' BUILD '.bgYellow.black; msg = '['.yellow + getTiming().yellow + '] → '.yellow + msg.yellow; }
    if (type == 'dev') { type = ' DEV '.bgWhite.black; msg = '['.grey + getTiming().grey + '] → '.grey + msg.grey; }
    if (type == 'welcome')        { type = '   YO!   '.bgCyan.black; msg = msg.cyan; }
    if (type == 'welcome-blank')  { type = '         '.bgCyan.black; msg = msg.cyan; }
    if (type == 'help') { type = ' HELP '.bgCyan.black; msg = msg.cyan; }
    console.log('['.grey + init.grey + ']'.grey + ' → ' + type + ' ' + msg);
};

// Var for set if it's local or global compilation
if (argv.prod) {                                                  // Catch Prod flag
    argv.global             = true,
    argv.stylGlobalMsg      = 'You force prod'.bold;
    argv.stylMaps           = false
} else if (env.statement == 'prod') {                             // Catch Prod env
    argv.global             = true,
    agrv.stylGlobalMsg      = 'Your environement is prod';
    argv.stylMaps           = false
} else {                                                          // Catch Dev env
    argv.global             = false,
    argv.stylGlobalMsg      = 'Your environement is dev, you compile locally';
    argv.stylMaps           = env.style.maps.use;
};

// Set basic env
argv.stylType = ''

// Separate simple tasks
gulp.task( 'welcome', require('./gulp-tasks/welcome')
    (gulp, plugins, env, consoleLog));

gulp.task('stylus', require('./gulp-tasks/stylus')
    (gulp, plugins, env, consoleLog, stylus, plumber, notify, gulpif, beep, autoprefixer, lost, poststylus, sourcemaps, argv, rename));

gulp.task('watchers', require('./gulp-tasks/watch')
    (gulp, plugins, env, consoleLog, argv));

// Simple task
gulp.task('destroy', require('./gulp-tasks/destroy')
    (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif));

gulp.task('destroy-css-init', require('./gulp-tasks/destroy-css')
    (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif));

gulp.task('destroy-js-init', require('./gulp-tasks/destroy-js')
    (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif));

gulp.task('destroy-img-init', require('./gulp-tasks/destroy-img')
    (gulp, plugins, env, consoleLog, argv, del, emptyDir, prompt, gulpif));

// Build tasks
gulp.task('builds', require('./gulp-tasks/builds')
    (gulp, plugins, env, consoleLog, argv));

gulp.task('build-css-init', require('./gulp-tasks/build-css')
    (gulp, plugins, env, consoleLog, argv));

gulp.task('build-js-init', require('./gulp-tasks/build-js')
    (gulp, plugins, env, consoleLog, argv));

gulp.task('build-img-init', require('./gulp-tasks/build-img')
    (gulp, plugins, env, consoleLog, argv));

// Others
gulp.task('js-components', require('./gulp-tasks/js-components')
    (gulp, plugins, env, consoleLog, stylus, plumber, notify, gulpif, beep, sourcemaps, argv, rename));

gulp.task('js-lib', require('./gulp-tasks/js-lib')
    (gulp, plugins, env, consoleLog, stylus, plumber, notify, gulpif, beep, sourcemaps, argv, rename));

gulp.task('img-optim', require('./gulp-tasks/img-optim')
    (gulp, plugins, env, consoleLog, plumber, notify, gulpif, beep, argv, imagemin));

gulp.task( 'end', require('./gulp-tasks/end')
    (gulp, plugins, env, consoleLog));

gulp.task('helper', require('./gulp-tasks/help')
    (gulp, plugins, env, consoleLog));



// Multiples tasks launcher
// Default = help
gulp.task('default', gulp.series('welcome', gulp.parallel('helper')));              // Help Task (default)
gulp.task('help', gulp.series('welcome', gulp.parallel('helper')));                 // Help Task

// Watch
gulp.task('watch', gulp.series('welcome', gulp.parallel('watchers')));              // Launch watcher

// Build tasks
gulp.task('build', gulp.series('welcome', 'builds', gulp.parallel('stylus', 'js-components', 'js-lib', 'img-optim'), 'end'));   // Build all
gulp.task('build-css', gulp.series('welcome', 'build-css-init', gulp.parallel('stylus'), 'end'));                               // Build CSS
gulp.task('build-js', gulp.series('welcome', 'build-js-init', gulp.parallel('js-components', 'js-lib'), 'end'));                // Build JD
gulp.task('build-img', gulp.series('welcome', 'build-img-init', gulp.parallel('img-optim'), 'end'));                            // Build Img

// Clean tasks
gulp.task('clean', gulp.series('welcome', gulp.series('destroy')));                 // Clean all
gulp.task('clean-css', gulp.series('welcome', gulp.series('destroy-css-init')));    // Clean CSS
gulp.task('clean-js', gulp.series('welcome', gulp.series('destroy-js-init')));      // Clean JS
gulp.task('clean-img', gulp.series('welcome', gulp.series('destroy-img-init')));    // Clean Img
